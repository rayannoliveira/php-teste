
<?php

    //Recupera do Formulário
    //$cep = $_POST["txtcep"];
    //Chamada ao servico REST
    //URL de servico
    //Aqui indicamos a URL que iremos utilizar para acessar o serviço
    $url = "https://data.melbourne.vic.gov.au/resource/vh2v-4nfs.json";
    //Inicializa cURL para uma URL.
    //Prepara o CURL para criar um conexao que será estabelecida entre a nossa aplicação e o nosso serviço
    $ch = curl_init($url);
    //Marca que vai receber string
    //Existem várias opções de setup, aqui será utilizado apenas que iremos receber uma string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //Inicia a conexão
    //Dispara a nossa solicitação, é como se tivessemos dando enter na barra de endereços do navegador
    $resposta = curl_exec($ch);
    //Fecha a conexão
    //Após receber os dados, fechamos a conexao
    curl_close($ch);
    //Usando a decodificação em JSON
    //A forma do PHP trabalhar com JSON é muito simples
    //Ele transforma os dados recebidos em um objeto ou em uma lista de objetos
    $listadeobjetos = json_decode($resposta);

    $listadevagas= $listadeobjetos;
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Home - Brand</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i">
    <link rel="stylesheet" href="assets/fonts/simple-line-icons.min.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-v2-Modal--Full-with-Google-Map.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <link rel="stylesheet" href="assets/css/smoothproducts.css">
   <style>
  #map {
    height: 100%;
  }
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
</style>
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar" style="background-color: rgb(255,255,255);">
        <div class="container"><a class="navbar-brand logo" href="#">SmartParking Dashboard</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"
                id="navcol-1">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="index.html">Home</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <main class="page landing-page">
        <section class="clean-block clean-info dark" style="background-color: rgb(255,255,255);">
            <div class="container" style="filter: brightness(100%);">
                <h3></h3>
                <div class="col" style="width: 1120px;height: 340px;">
                    <div id="map"></div>
                      <script>
      var map;
      function initMap() {
        map = new google.maps.Map(
            document.getElementById('map'),
            {center: new google.maps.LatLng(-37.808697589247046, 144.97193891371268), zoom: 12});
<?php 
  foreach ($listadevagas as $e) { ?>
              var ponto = new google.maps.LatLng(<?php echo $e->lat; ?>,<?php echo $e->lon; ?>);
              //Função marker
              marker = new google.maps.Marker({
              position: ponto,
              map: map,
              title:'<?php echo $e->status;?>'});
 
              marker.addListener('click', function(event) {              
              alert("Localização Selecionada");
               document.getElementById("demo").value = "<?php echo $e->lat; ?>";
               document.getElementById("demo2").value = "<?php echo $e->lon; ?>";
            });
 <?php
  }
?> 
      }
    </script>
                </div>
                <div class="form-group">
                    <div class="row align-items-center">
                        <div class="col-md-6" style="padding-bottom: 151px;background-color: #ffffff;">
                            <form action="controler/buscaController.php" method="post">
                                <h3></h3>
                            <h5>Localização Selecionada</h5>
                            <input class="form-control-lg" id="demo" name="lat" type="text" style="margin:1px;width:250px;padding-right:17px;padding-top:18px;padding-bottom:13px;padding-left:15px;">
                            <input class="form-control-lg" id="demo2" name="long" type="text" style="margin:1px;width:250px;padding-right:17px;padding-top:18px;padding-bottom:13px;padding-left:15px;">
                            <h5>Data inicial</h5>
                            <input class="form-control-lg" name="data_incial" type="date">
                            <h5>Data final</h5>
                            <div class="getting-started-info">
                                <p></p>
                            </div><input class="form-control-lg" name="data_final" type="date">
                            <h5>Defina o raio da busca</h5>
                            <input class="form-control-lg" id="demo" name="raio" type="text" style="margin:1px;width:250px;padding-right:17px;padding-top:18px;padding-bottom:13px;padding-left:15px;">
                            <h3></h3>
                              <button onclick="myFunction()"
                            class="btn btn-primary" type="submit" style="margin:1px;width:250px;padding-right:17px;padding-top:18px;padding-bottom:13px;padding-left:15px;background-color:rgba(0,123,255);">Procurar
                        </button>
                        </div>
                            </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script src="assets/js/smoothproducts.min.js"></script>
    <script src="assets/js/theme.js"></script>
    <script src="assets/js/Contact-Form-v2-Modal--Full-with-Google-Map.js"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBLssRAx8ZUNFjM9FS7tKcbY0JeLmQHhk&callback=initMap">
    </script>
    <script>
function myFunction() {
  alert("O processamento de dados poderá demorar");
}
</script>
</body>

</html>